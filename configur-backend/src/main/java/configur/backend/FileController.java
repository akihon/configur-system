package configur.backend;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.mongodb.client.MongoClient;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.multipart.CompletedFileUpload;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class FileController {

    private final MongoClient mongoClient;

    @Inject
    public FileController(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Post(value = "/file-upload", 
    produces = MediaType.APPLICATION_JSON, 
    consumes = MediaType.MULTIPART_FORM_DATA)
    public HttpResponse<String> fileUpload(CompletedFileUpload file) {
        File tempFile;
        Path path = null;
        try {
            tempFile = File.createTempFile(file.getFilename(), ".csv");
            path = Paths.get(tempFile.getAbsolutePath());
            Files.write(path, file.getBytes());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        List<PersonData> people;

        try {
            MappingIterator<PersonData> personIter = new CsvMapper().readerWithTypedSchemaFor(PersonData.class).readValues(path.toFile());
            people = personIter.readAll();
            people.remove(0);

            //store the list of people in the data store
            var aka = mongoClient
                    .getDatabase("data")
                    .getCollection("people", PersonData.class)
                    .insertMany(people)
                    .wasAcknowledged();
            if(aka)  {
                Files.delete(path);
                return HttpResponse.accepted().body("uploaded");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.delete(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return HttpResponse.accepted().body("Not uploaded");
    }

}