package configur.backend;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.gson.annotations.SerializedName;

@JsonPropertyOrder({"_id", "firstName", "lastName", "email", "phoneNumber", "address1", "address2", "county", "postcode", "sales"})
public class PersonData {

    @SerializedName(value = "id")
    public String _id;
    @SerializedName(value = "firstName")
    public String firstName;
    @SerializedName(value = "lastName")
    public String lastName;
    @SerializedName(value = "email")
    public String email;
    @SerializedName(value = "phoneNumber")
    public String phoneNumber;
    @SerializedName(value = "address1")
    public String address1;
    @SerializedName(value = "address2")
    public String address2;
    @SerializedName(value = "county")
    public String county;
    @SerializedName(value = "postcode")
    public String postcode;
    @SerializedName(value = "sales")
    public String sales;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }
}
