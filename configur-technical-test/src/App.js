import React from 'react';
import './App.css';
import './modules/FileUpload'; 
import FileUpload from './modules/FileUpload';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <body className="App-body">
        <p>
          Choose a file to upload!
        </p>
        <FileUpload />
      </body>
    </div>
  );
}

export default App;
